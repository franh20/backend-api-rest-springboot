package com.bolsadeideas.springboot.backend.apirest.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bolsadeideas.springboot.backend.apirest.models.entity.Cliente;
import com.bolsadeideas.springboot.backend.apirest.models.services.IClienteService;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class ClienteRestController {
	
	private static final Log LOG = LogFactory.getLog(ClienteRestController.class); 
	
	@Autowired
	private IClienteService clienteService;

	@GetMapping("/clientes/page/{page}")
	public Page<Cliente> index(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 4, Sort.by("nombre").ascending().and(Sort.by("apellido").descending()));
		return clienteService.findAll(pageable);
	}

	@GetMapping("/clientes")
	public ResponseEntity<?> index() {
		LOG.info("... called method ClienteRestController.index ...");
		List<Cliente> clientes = null;
		Map<String, Object> response = new HashMap<>();
		try {
			clientes = clienteService.findAll();
		}catch(DataAccessException e) {
			response.put( "mensaje", "Error al realizar la consulta a la base de datos");
			response.put( "error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()) );
			return new ResponseEntity< Map<String, Object> >(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Cliente>>(clientes, HttpStatus.OK);
	}
	
	@GetMapping("/clientes/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> show(@PathVariable Long id) {
		LOG.info("... called method ClienteRestController.show ...");
		Cliente cliente = null;
		Map<String, Object> response = new HashMap<>();		
		try {
			cliente = clienteService.findById(id);
		} catch(DataAccessException e){
			response.put( "mensaje", "Error al realizar la consulta a la base de datos");
			response.put( "error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()) );
			return new ResponseEntity< Map<String, Object> >(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(cliente == null) {
			response.put(
					"mensaje", 
					"El cliete ID: ".concat(id.toString()).concat(" no existe en la base de datos!") );
			return new ResponseEntity< Map<String, Object> >(response, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Cliente>(cliente, HttpStatus.OK); 
	}
	
	@PostMapping("/clientes")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> create(@RequestBody Cliente cliente) {
		LOG.info("... called method ClienteRestController.create ...");		
		Cliente newCliente = null;
		Map<String, Object> response = new HashMap<>();
		try {
			newCliente = clienteService.save(cliente);
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar el insert en la base de datos");
			response.put( "error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()) );
			return new ResponseEntity< Map<String, Object> >(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "El cliente ha sido creado con exito!");
		response.put("cliente", newCliente);
		return new ResponseEntity< Map<String, Object> >(response, HttpStatus.CREATED);
	}
	
	@PutMapping("/clientes/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> update(@RequestBody Cliente cliente, @PathVariable Long id) {
		LOG.info("... called method ClienteRestController.update ...");
		Cliente clienteActual = null;
		Map<String, Object> response = new HashMap<>();
		try {
			clienteActual = clienteService.findById(id);
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar el update en la base de datos");
			response.put( "error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()) );
			return new ResponseEntity< Map<String, Object> >(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if ( clienteActual == null) {
			response.put("mensaje", "El cliete ID: ".concat(id.toString()).concat(" no existe en la base de datos!") );
			return new ResponseEntity< Map<String, Object> >(response, HttpStatus.NOT_FOUND); 
		}
		
		Cliente clientUpdate = null;
		try {
			clienteActual.setNombre(cliente.getNombre());
			clienteActual.setApellido(cliente.getApellido());
			clienteActual.setEmail(cliente.getEmail());
			clientUpdate = clienteService.save(clienteActual);
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar el update en la base de datos");
			response.put( "error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()) );
			return new ResponseEntity< Map<String, Object> >(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("cliente", clientUpdate);
		response.put("mensaje", "El cliente ha sido actualizado con exito! ");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	} 
	
	@DeleteMapping("/clientes/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		LOG.info("... called method ClienteRestController.delete ...");
		Map<String, Object> response = new HashMap<>();
		try {
			clienteService.delete(id);
		}catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar el update en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()) );
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "El cliente ID: " + id.toString() + " ha sido eliminado con exito!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK );
	}
}
